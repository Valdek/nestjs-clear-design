import { Injectable, Logger } from '@nestjs/common';
import { IActivitiesService } from '../interfaces/activities.service.interface';

import { InjectEventEmitter } from 'nest-emitter';
import { TActivitiesEmiter } from '../../app/emiters/activities.emiter';
import { ActivityEvent } from '../../app/events/activities.events';

@Injectable()
export class ActivitiesService implements IActivitiesService {
  constructor(
    @InjectEventEmitter() private readonly emitter: TActivitiesEmiter
  ) { }
  onModuleInit() {
    this.emitter.on(ActivityEvent.ACTIVITY_EXECUTED, (name: string, data: any) => this.execActivity(name, data));
  }

  private readonly logger: Logger = new Logger('Activities')

  execActivity(name: string, data: any): void {
    this.logger.log(`${name} | ${data}`)
  }

}
