import { ActivityEvent } from "../../app/events/activities.events";

export interface IActivitiesEvents {
    [ActivityEvent.ACTIVITY_EXECUTED]: (name: string, data: any) => void;
}