export interface IActivitiesService {
  execActivity(name: string, data: any): void;
}