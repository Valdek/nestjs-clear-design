import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';
import { IActivitiesEvents } from '../../domain/interfaces/activities-events.interface';

export type TActivitiesEmiter = StrictEventEmitter<EventEmitter, IActivitiesEvents>;