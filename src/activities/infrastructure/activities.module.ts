import { Module } from '@nestjs/common';
import { ActivitiesServiceProvider } from './providers/activities.service.provider';


@Module({
  imports: [
  ],
  providers: [
    ActivitiesServiceProvider
  ],
})

export class ActivitiesModule { }
