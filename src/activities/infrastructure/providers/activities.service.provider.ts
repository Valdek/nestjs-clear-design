import { ActivitiesService } from '../../domain/servises/activities.service';

export const ACTIVITIES_SERVICE_TOKEN = 'ActivitiesServiceProvider'

export const ActivitiesServiceProvider = {
    provide: ACTIVITIES_SERVICE_TOKEN,
    useClass: ActivitiesService
}