import { Module } from '@nestjs/common';
import { EntriesController } from '../app/controllers/entries.controller';
import { EntriesRepositoryProvider } from './providers/entries.repository.provider';
import { EntriesServiceProvider } from './providers/entries.service.provider';

@Module({
  controllers: [
    EntriesController
  ],
  providers: [
    EntriesRepositoryProvider,
    EntriesServiceProvider,
  ],
  exports: [
    EntriesServiceProvider
  ],
})

export class EntriesModule { }
