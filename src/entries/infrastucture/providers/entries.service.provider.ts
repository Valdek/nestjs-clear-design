import { EntriesService } from '../../domain/servises/entries.service';

export const ENTRIES_SERVICE = 'EntriesServiceProvider' as const

export const EntriesServiceProvider = {
    provide: ENTRIES_SERVICE,
    useClass: EntriesService
}