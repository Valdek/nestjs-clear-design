import { Connection } from 'typeorm';
import { Entry } from '../../domain/entities/entry.entity';
import { EntriesRepository } from '../../domain/repositories/entries.repository';

export const ENTRIES_REPOSITORY = 'EntriesRepositoryProvider' as const

export const EntriesRepositoryProvider = {
    provide: ENTRIES_REPOSITORY,
    useFactory: (connection: Connection) => connection.getCustomRepository(EntriesRepository),
    inject: [Connection]
}