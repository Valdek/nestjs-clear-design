import { Body, Controller, Delete, Get, Inject, Param, Post } from '@nestjs/common';
import { CreateEntryDto } from '../../domain/dto/req/create-entry.dto';
import { IEntriesService } from '../../domain/interfaces/entries.service.interface';
import { IEntry } from '../../domain/interfaces/entry.interface';
import { ENTRIES_SERVICE } from '../../infrastucture/providers/entries.service.provider';


@Controller('entries')
export class EntriesController {

  constructor(
    @Inject(ENTRIES_SERVICE) private readonly entriesService: IEntriesService
  ) { }

  @Get()
  async findAll(): Promise<IEntry[]> {
    return this.entriesService.findAll();
  }

  @Post()
  async create(@Body() createEntryDto: CreateEntryDto) {
    await this.entriesService.store(createEntryDto);
  }

  @Delete(':entryId')
  delete(@Param('entryId') entryId) {
    return this.entriesService.deleteById(entryId);
  }
}
