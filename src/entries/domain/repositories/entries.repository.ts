import { Entry } from "../entities/entry.entity";
import { EntityRepository, Repository } from "typeorm";
import { IEntry } from "../interfaces/entry.interface";
import { IEntriesRepository } from "../interfaces/entries.repository.interface";

@EntityRepository(Entry) 
export class EntriesRepository extends Repository<IEntry> implements IEntriesRepository {
    async getAll() {
        return await this.find()
    }
}