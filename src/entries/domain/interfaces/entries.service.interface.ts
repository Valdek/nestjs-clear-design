import { Entry } from "../entities/entry.entity";
import { CreateEntryDto } from "../dto";

export interface IEntriesService {
  findAll(): Promise<Entry[]>;
  store(dto: CreateEntryDto): Promise<Entry>;
  deleteById(id: string): Promise<void>;
}