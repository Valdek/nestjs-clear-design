import { Repository } from "typeorm";
import { IEntry } from "./entry.interface";

export interface IEntriesRepository extends Repository<IEntry> {
    getAll(): Promise<IEntry[]>;
}