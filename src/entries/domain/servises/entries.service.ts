import { Inject, Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectEventEmitter } from 'nest-emitter';
import { Repository } from 'typeorm';
import { TActivitiesEmiter } from '../../../activities/app/emiters/activities.emiter';
import { CreateEntryDto } from '../dto';
import { IEntry } from '../interfaces/entry.interface';
import { ENTRIES_REPOSITORY } from '../../infrastucture/providers/entries.repository.provider';
import { IEntriesService } from '../interfaces/entries.service.interface';
import { ActivityEvent } from '../../../activities/app/events/activities.events';

@Injectable()
export class EntriesService implements IEntriesService {
  constructor(
    @Inject(ENTRIES_REPOSITORY) private readonly entryRepository: Repository<IEntry>,

    @InjectEventEmitter() private readonly activitiesEmiter: TActivitiesEmiter,
  ) { }

  async findAll() {
    const entries = await this.entryRepository.find();

    this.activitiesEmiter.emit(ActivityEvent.ACTIVITY_EXECUTED, 'entries-fetched', entries.map(entry => entry.id))

    return entries
  }

  async store(dto: CreateEntryDto) {
    const createdEntry = await this.entryRepository.save(dto);

    if (!createdEntry)
      throw new InternalServerErrorException('Failed to create entry')

    this.activitiesEmiter.emit(ActivityEvent.ACTIVITY_EXECUTED, 'entry-created', createdEntry.id)

    return createdEntry
  }

  async deleteById(id: string) {
    const result = await this.entryRepository.delete(id);

    if (result.affected > 0)
      this.activitiesEmiter.emit(ActivityEvent.ACTIVITY_EXECUTED, 'entry-deleted', id)
    else
      throw new InternalServerErrorException('Failed to delete entry')
  }

}
