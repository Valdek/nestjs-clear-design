import { IsLowercase, IsString, Length } from 'class-validator';

export class CreateEntryDto {
  @IsString() @Length(3, 128) 
  readonly title: string;

  @IsString() @Length(1, 65650) 
  readonly body: string;
}
