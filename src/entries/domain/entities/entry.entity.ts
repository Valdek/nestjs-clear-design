import { Column, Entity, PrimaryGeneratedColumn, CreateDateColumn } from 'typeorm';

@Entity()
export class Entry {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  body: string;

	@CreateDateColumn({ nullable: true, type: 'timestamp', default: () => 'LOCALTIMESTAMP' })
  created_at: string;

}
