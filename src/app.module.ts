import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConnectModule } from '../database/connect/connect.module';
import { EntriesModule } from './entries/infrastucture/entries.module';
import { ActivitiesModule } from './activities/infrastructure/activities.module';
import { NestEmitterModule } from 'nest-emitter';
import { EventEmitter } from 'events';
@Module({
  imports: [
    NestEmitterModule.forRoot(new EventEmitter()),
    ConnectModule,
    EntriesModule,
    ActivitiesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
