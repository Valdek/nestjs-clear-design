import * as dotenv from 'dotenv';
import * as fs from 'fs';

import entities from './config.entities';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export class ConfigService {
	private readonly envConfig: Record<string, string>;

	constructor(filePath: string) {
		this.envConfig = dotenv.parse(fs.readFileSync(filePath));
	}

	get(key: string): string {
		return this.envConfig[key];
	}

	getOrDef(key: string, def: any): any {
		if (this.get(key)) {
			return this.get(key);
		} else {
			return def;
		}
	}

	getPostgresConfig(): TypeOrmModuleOptions {
		return {
			type: this.getOrDef('DB_TYPE', 'postgres'),
			host: this.getOrDef('DB_HOST', 'nestjs-postgres'),
			port: this.getOrDef('DB_PORT', 5432),
			username: this.getOrDef('DB_USERNAME', 'postgres'),
			password: this.getOrDef('DB_PASSWORD', 'postgres'),
			database: this.getOrDef('DB_DATABASE', 'metu'),
			entities,
			synchronize: this.getOrDef('DB_SYNC', true),
			keepConnectionAlive: true,
		};
	}

	getAdminPanelUrl(): string {
		const url = this.get('ADMIN_PANEL_URL') || `http://0.0.0.0:3000`
		return url
	}

	getErrorCode(key: string): number {

		switch (key) {
			case 'unique_violation': return 23505;
			default: return null;
		}

	}
}
