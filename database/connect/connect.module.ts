import { Module } from '@nestjs/common';
import { ConfigModule } from '../../core/config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from '../../core/config/config.service';
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions';

@Module({

  imports: [
    ConfigModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => config.getPostgresConfig() as PostgresConnectionOptions,
    }),
  ],

})
export class ConnectModule { }
